// stdlib functionality
#include <iostream>
// ROOT functionality
#include <TFile.h>
#include <TH1D.h>
// ATLAS EDM functionality
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODJet/JetContainer.h"
// Add a submodule for JetSelectionHelper
#include "JetSelectionHelper/JetSelectionHelper.h"
// jet calibration
#include "AsgTools/AnaToolHandle.h"
#include "JetCalibTools/IJetCalibrationTool.h"

int main(int argc, char** argv) {

  // initialize the xAOD EDM
  xAOD::Init();

  // get the number of events in the file to loop over
  Long64_t numEntries(-1);
  if(argc >= 3) numEntries = std::atoi(argv[2]);
  
  asg::AnaToolHandle<IJetCalibrationTool> JetCalibrationTool_handle;

  JetCalibrationTool_handle.setTypeAndName("JetCalibrationTool/MyCalibrationTool");

  JetCalibrationTool_handle.setProperty("JetCollection","AntiKt4EMTopo"                                                  );
  JetCalibrationTool_handle.setProperty("ConfigFile"   ,"JES_MC16Recommendation_Consolidated_EMTopo_Apr2019_Rel21.config");
  JetCalibrationTool_handle.setProperty("CalibSequence","JetArea_Residual_EtaJES_GSC_Smear"                              );
  JetCalibrationTool_handle.setProperty("CalibArea"    ,"00-04-82"                                                       );
  JetCalibrationTool_handle.setProperty("IsData"       ,false                                                            );

  JetCalibrationTool_handle.retrieve();


  // open the input file
  TString inputFilePath = "/home/atlas/Bootcamp/Data/DAOD_EXOT27.17882744._000026.pool.root.1";
  if(argc >= 2) inputFilePath = argv[1];
  xAOD::TEvent event;
  std::unique_ptr< TFile > iFile ( TFile::Open(inputFilePath, "READ") );
  if(!iFile) return 1;
  event.readFrom( iFile.get() );

  // Histograms to store njets and mjj
  TH1D *NJets = new TH1D("NJets", "NJets", 20, 0, 20);
  NJets->SetYTitle("Count");
  NJets->SetXTitle("N_Jets");
  TH1D *MJJ   = new TH1D("MJJ", "MJJ", 100, 0, 500);
  MJJ->SetYTitle("Count");
  MJJ->SetXTitle("M_bb");

  TH1D *NJets_kin = new TH1D("NJets_kin", "NJets_kin", 20, 0, 20);
  NJets_kin->SetYTitle("Count");
  NJets_kin->SetXTitle("N_Jets");
  TH1D *MJJ_kin   = new TH1D("MJJ_kin", "MJJ_kin", 100, 0, 500);
  MJJ_kin->SetYTitle("Count");
  MJJ_kin->SetXTitle("M_bb");


  // add jet selection helper
  JetSelectionHelper jet_selector;

  // for counting events
  unsigned count = 0;

  // get the number of events in the file to loop over
  if(numEntries == -1) numEntries = event.getEntries();
  std::cout << "Processing " << numEntries << " events" << std::endl;

  // primary event loop
  for ( Long64_t i=0; i<numEntries; ++i ) {

    // Load the event
    event.getEntry( i );

    // Load xAOD::EventInfo and print the event info
    const xAOD::EventInfo * ei = nullptr;
    event.retrieve( ei, "EventInfo" );
    std::cout << "Processing run # " << ei->runNumber() << ", event # " << ei->eventNumber() << std::endl;

    // retrieve the jet container from the event store
    const xAOD::JetContainer* jets = nullptr;
    event.retrieve(jets, "AntiKt4EMTopoJets");

    // Save the jets to this vector
    std::vector<xAOD::Jet> jets_pass;
    std::vector<xAOD::Jet> jets_kin;

    // loop through all of the jets and make selections with the helper
    for(const xAOD::Jet* jet : *jets) {

      // calibrate the jet
      xAOD::Jet *calibratedjet;
      JetCalibrationTool_handle->calibratedCopy(*jet,calibratedjet);	

      if (jet_selector.isJetGood(calibratedjet))
      {	
        // print the kinematics of each jet in the event
        std::cout << "Jet : pt=" << calibratedjet->pt() << "  eta=" << calibratedjet->eta() << "  phi=" << calibratedjet->phi() << "  m=" << calibratedjet->m() << std::endl;
      
         // Save the jets to the jets_pass vector
         jets_pass.push_back(*calibratedjet);

	 if(calibratedjet->pt() > 50000) jets_kin.push_back(*calibratedjet);
       }
      // cleanup
      delete calibratedjet;
    }

    // Add the jets to the NJets Histogram
    NJets->Fill( jets_pass.size() );
    NJets_kin->Fill( jets_kin.size() );

    // Add the two leading jets to the MJJ Histogram
    if ( jets_pass.size()>=2 ) {
      MJJ->Fill( (jets_pass.at(0).p4()+jets_pass.at(1).p4()).M()/1000. );
      MJJ_kin->Fill( (jets_kin.at(0).p4()+jets_kin.at(1).p4()).M()/1000. );
    }

    // counter for the number of events analyzed thus far
    count += 1;
  }

  // Write to a root file to see the output
  TFile *f = new TFile("myOutputFile.root", "RECREATE");

  NJets->Write();
  MJJ->Write();
  NJets_kin->Write();
  MJJ_kin->Write();


  // REMEMBER TO CLOSE THE FILE
  f->Close();


  // exit from the main function cleanly
  return 0;
}

